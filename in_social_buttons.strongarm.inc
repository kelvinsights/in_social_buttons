<?php
/**
 * @file
 * in_social_buttons.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function in_social_buttons_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'shorten_bitly_key';
  $strongarm->value = 'R_6589fa84c3f5faded11ba5ce5926b727';
  $export['shorten_bitly_key'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'shorten_bitly_login';
  $strongarm->value = 'kkwong00';
  $export['shorten_bitly_login'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'shorten_service';
  $strongarm->value = 'bit.ly';
  $export['shorten_service'] = $strongarm;

  return $export;
}
