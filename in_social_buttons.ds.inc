<?php
/**
 * @file
 * in_social_buttons.ds.inc
 */

/**
 * Implements hook_ds_custom_fields_info().
 */
function in_social_buttons_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'social_buttons_example';
  $ds_field->label = 'Example: Social buttons';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '*|revision';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<?php 

$path=drupal_get_path_alias(\'node/\'.$entity->nid);
$title = $entity->title;
$url="http://".$_SERVER[\'SERVER_NAME\']."/".$path;
		
//Facebook Like
//http://developers.facebook.com/docs/reference/plugins/like/
print \'
<div class="fb-like" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></div>
\';

//Twitter
//https://dev.twitter.com/docs/tweet-button
print \'
<a href="https://twitter.com/share" class="twitter-share-button" data-url="\'.shorten_url($url).\'" data-counturl="\'.$url.\'" data-text="\'.$title.\' \'.shorten_url($url).\'">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
\';
		
//Linkedin
//http://developer.linkedin.com/share-plugin
//https://developer.linkedin.com/share-plugin-visual-attributes
print \'
<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
<script type="IN/Share" data-counter="right"></script>
\';

//Google +1
//https://developers.google.com/+/plugins/+1button/
print \'
<div class="gplus"><g:plusone size="medium"></g:plusone></div>
\';

//Weibo
//http://open.weibo.com/sharebutton
print \'
<div class="weibo"><script type="text/javascript" charset="utf-8">
	(function(){
	  var _w = 45 , _h = 16;
	  var param = {
	    url:location.href,
	    type:\\\'3\\\',
	    count:\\\'1\\\', /**是否显示分享数，1显示(可选)*/
	    appkey:\\\'\\\', /**您申请的应用appkey,显示分享来源(可选)*/
	    title:\\\'\\\', /**分享的文字内容(可选，默认为所在页面的title)*/
	    pic:\\\'\\\', /**分享图片的路径(可选)*/
	    ralateUid:\\\'\\\', /**关联用户的UID，分享微博会@该用户(可选)*/
		language:\\\'zh_cn\\\', /**设置语言，zh_cn|zh_tw(可选)*/
	    rnd:new Date().valueOf()
	  }
	  var temp = [];
	  for( var p in param ){
	    temp.push(p + \\\'=\\\' + encodeURIComponent( param[p] || \\\'\\\' ) )
	  }
	  document.write(\\\'<iframe allowTransparency="true" frameborder="0" scrolling="no" src="http://hits.sinajs.cn/A1/weiboshare.html?\\\' + temp.join(\\\'&\\\') + \\\'" width="\\\'+ _w+\\\'" height="\\\'+_h+\\\'"></iframe>\\\')
	})()
	</script></div>
\';


?>',
      'format' => 'ds_code',
    ),
    'use_token' => 0,
  );
  $export['social_buttons_example'] = $ds_field;

  return $export;
}
